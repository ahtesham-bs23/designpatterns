﻿using DesignPatterns.Structural.Adapter;
using Abstraction = DesignPatterns.Structural.Bridge.Abstraction;
using DesignPatterns.Structural.Bridge.ConcreteImplementor;
using DesignPatterns.Structural.Bridge.RefinedAbstarction;
using DesignPatterns.Structural.Composite.Leaf;
using DesignPatterns.Structural.Composite.Composite;
using DesignPatterns.Structural.Decorator.Component;
using DesignPatterns.Structural.Decorator.ConcreteComponent;
using DesignPatterns.Structural.Decorator.Decorator;
using DesignPatterns.Structural.Decorator.ConcreteDecorator;
using DesignPatterns.Behavioral.ChainOfResponsibility;
using DesignPatterns.Behavioral.Mediator;
using DesignPatterns.Behavioral.Observer;

#region Structural
//** Adapter
//var employeeManager = new EmployeeAdapter();
//var employees = employeeManager.GetEmployees();
//foreach (var employee in employees)
//{
//    var emp = employee as SmartEmployee;
//    Console.WriteLine($"Name: {emp.FullName} & salary: ${emp.SalaryInUSDollar}");
//}

//** Bridge
//Abstraction.BookingManager tour = new TravelByAir();
//tour.destinationManager = new KuakatBookingManager();
//tour.BookMyTour();

//Console.ReadKey();

////** Composite
//var kacchi = new FoodMenu("Kacchi");
//var beef = new FoodMenu("Beef");
//var softDrik = new FoodMenu("Soft Drik");
//var boildEgg = new FoodMenu("Boild Egg");
//var rice = new FoodMenu("plain rice");
//var SetMenu1 = new Platter("Set Menu 1")
//{
//    Items = { rice, beef, boildEgg, softDrik }
//};

//kacchi.GetMenu();

//Console.ReadKey();


//** Decorator

//IMobile mobile = new Samsung();
//MobileDecorator decorator = new DiscountManager(mobile);
//Console.WriteLine($"{decorator.Name}, discounted price : {decorator.GetDiscountedPrice().ToString()}");
//Console.ReadKey();

#endregion

#region Behavioral

#region Chain of responsibility
//Handler handler1 = new ConcreteHandler1();
//Handler handler2 = new ConcreteHandler2();
//handler1.SetNext(handler2);
//handler1.HandleRequest("Type2");

#endregion

#region Mediator

//var c1 = new Component1();
//var c2 = new Component2();
//var mediator = new ConcreteMediator(c1, c2);
//c1.DoA();
#endregion

#region Observer
var subject = new Subject();
var observer1 = new ConcreteObserver("Observer1");
var observer2 = new ConcreteObserver("Observer2");

subject.Subscribe(observer1);
subject.Subscribe(observer2);
subject.Notify("Event Occurred!");

#endregion

#endregion