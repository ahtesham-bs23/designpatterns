﻿using DesignPatterns.Structural.Composite.Component;
using DesignPatterns.Structural.Composite.Leaf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Composite.Composite
{
    public class Platter: IFoodMenu
    {
        public Platter(string name)
        {
            Name = name;
            Items = new List<FoodMenu>();
        }
        public string Name { get; set; }
        public List<FoodMenu> Items { get; set; }
        public void GetMenu()
        {
            Console.WriteLine($"{this.Name}");

            Console.WriteLine("---------Items--------");
            foreach ( var item in Items )
            {
                Console.WriteLine($"Item: {item.Name}");
            }
        }
    }
}
