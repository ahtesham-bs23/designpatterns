﻿using DesignPatterns.Structural.Composite.Component;

namespace DesignPatterns.Structural.Composite.Leaf
{
    public class FoodMenu : IFoodMenu
    {
        public FoodMenu(string name) { 
            Name = name;
        }
        public string Name { get; set; }
        public void GetMenu()
        {
            Console.WriteLine($"Item: {this.Name}");
        }
    }
}
