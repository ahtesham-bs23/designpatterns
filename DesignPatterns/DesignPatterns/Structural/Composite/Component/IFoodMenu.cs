﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Composite.Component
{
    public interface IFoodMenu
    {
        void GetMenu();
    }
}
