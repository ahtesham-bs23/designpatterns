﻿namespace DesignPatterns.Structural.Bridge.Abstraction
{
    public abstract class BookingManager
    {
        public IDestinationManager destinationManager;
        public abstract void BookMyTour();
    }
}
