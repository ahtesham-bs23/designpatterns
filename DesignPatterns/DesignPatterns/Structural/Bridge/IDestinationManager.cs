﻿namespace DesignPatterns.Structural.Bridge;
/// <summary>
/// Implementor Interface
/// </summary>
public interface IDestinationManager
{
    void BookDestination(string travelBy);
}
