﻿namespace DesignPatterns.Structural.Bridge.ConcreteImplementor;
public class CoxsBazarBookingManager : IDestinationManager
{
    /// <summary>
    /// ConcreteImplementor 
    /// </summary>
    public void BookDestination(string travelBy)
    {
        Console.WriteLine($"Tour to Cox's Bazar by {travelBy} booked.");
    }
}

