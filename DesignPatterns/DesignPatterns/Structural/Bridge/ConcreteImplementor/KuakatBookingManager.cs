﻿namespace DesignPatterns.Structural.Bridge.ConcreteImplementor;
public class KuakatBookingManager : IDestinationManager
{
    /// <summary>
    /// ConcreteImplementor 
    /// </summary>
    public void BookDestination(string travelBy)
    {
        Console.WriteLine($"Tour to Kuakata by {travelBy} booked.");
    }
}