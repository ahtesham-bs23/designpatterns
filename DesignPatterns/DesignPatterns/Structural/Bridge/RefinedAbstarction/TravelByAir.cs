﻿using DesignPatterns.Structural.Bridge.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Bridge.RefinedAbstarction
{
    public class TravelByAir : Abstraction.BookingManager
    {
        public override void BookMyTour()
        {
            destinationManager.BookDestination("Air");
        }
    }
}
