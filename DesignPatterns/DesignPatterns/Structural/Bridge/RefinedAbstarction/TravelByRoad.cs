﻿namespace DesignPatterns.Structural.Bridge.RefinedAbstarction
{
    public class TravelByRoad : Abstraction.BookingManager
    {
        public override void BookMyTour()
        {
            destinationManager.BookDestination("Road");
        }
    }
}
