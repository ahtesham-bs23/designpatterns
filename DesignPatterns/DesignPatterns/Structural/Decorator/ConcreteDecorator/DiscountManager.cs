﻿using DesignPatterns.Structural.Decorator.Component;
using DesignPatterns.Structural.Decorator.Decorator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Decorator.ConcreteDecorator
{
    public class DiscountManager: MobileDecorator
    {
        public DiscountManager(IMobile mobile):base(mobile)
        {
                
        }
        public override double GetDiscountedPrice()
        {
            return base.GetPrice() - (0.1 * base.GetPrice()) ;    
        }
    }
}
