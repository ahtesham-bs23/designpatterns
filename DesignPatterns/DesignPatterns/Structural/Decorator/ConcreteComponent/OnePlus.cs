﻿using DesignPatterns.Structural.Decorator.Component;

namespace DesignPatterns.Structural.Decorator.ConcreteComponent
{
    public sealed class OnePlus : IMobile
    {
        public string Name
        {
            get { return "One Plus"; }
        }
        public double GetPrice()
        {
            return 60000;
        }
    }
}
