﻿using DesignPatterns.Structural.Decorator.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Decorator.ConcreteComponent
{
    public sealed class Samsung: IMobile
    {
        public string Name {
            get { return "Samsung"; }
        }
        public double GetPrice() {
             return 70000;
        }
    }
}
