﻿using DesignPatterns.Structural.Decorator.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Decorator.Decorator
{
    public abstract class MobileDecorator:IMobile
    {
        private IMobile _mobile;
        public MobileDecorator(IMobile mobile)
        {
                _mobile = mobile;
        }
        public string Name { get { return _mobile.Name; } }
        public double GetPrice()
        {
            return _mobile.GetPrice();
        }
        public abstract double GetDiscountedPrice();
    }
}
