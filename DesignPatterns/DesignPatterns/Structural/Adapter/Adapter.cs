﻿namespace DesignPatterns.Structural.Adapter
{
    public class EmployeeAdapter : EmployeeManager, ISmartEmployeeManager
    {
        List<Object> smartEmployees = new List<object>();
        public override List<Object> GetEmployees()
        {
            var employees = base.GetEmployees();

            foreach (var employee in employees)
            {
                var emp = employee as Employee;
                smartEmployees.Add(new SmartEmployee
                {
                    FullName = $"{emp.FirstName} {emp.LastName}",
                    SalaryInUSDollar = emp.SalaryInTaka / 107
                });
            }
            return smartEmployees;
        }
    }
}
