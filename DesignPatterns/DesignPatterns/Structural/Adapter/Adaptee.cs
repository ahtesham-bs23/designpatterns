﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Adapter
{
    public class Employee
    {
        public string FirstName { get; set; }  
        public string LastName { get; set; }  
        public int SalaryInTaka { get; set; }  
    }
    public class EmployeeManager
    {
        List<Object> list = new List<object>();
        public EmployeeManager()
        {
            list.Add(new Employee { FirstName = "Shakibul", LastName = "Hasan", SalaryInTaka= 100000 });
            list.Add(new Employee { FirstName = "Mushfiqur", LastName = "Rahim", SalaryInTaka= 90000 });
            list.Add(new Employee { FirstName = "Mahmudullah", LastName = "Riad", SalaryInTaka= 80000 });
        }
        public virtual List<Object> GetEmployees() 
        { 
            return list;
        }
    }
}
