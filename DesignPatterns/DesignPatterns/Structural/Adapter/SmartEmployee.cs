﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Adapter
{
    public class SmartEmployee
    {
        public String FullName { get; set; } 
        public int SalaryInUSDollar { get; set; } 
    }
}
