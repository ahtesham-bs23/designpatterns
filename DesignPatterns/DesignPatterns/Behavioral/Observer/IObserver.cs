﻿namespace DesignPatterns.Behavioral.Observer
{
    public interface IObserver
    {
        void Publish(string message);
    }
}
