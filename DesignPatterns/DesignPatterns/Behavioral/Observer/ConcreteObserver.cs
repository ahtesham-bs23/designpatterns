﻿namespace DesignPatterns.Behavioral.Observer
{
    public class ConcreteObserver : IObserver
    {
        private string _name;

        public ConcreteObserver(string name)
        {
            _name = name;
        }

        public void Publish(string message)
        {
            Console.WriteLine($"{_name} received: {message}");
        }
    }
}
