﻿namespace DesignPatterns.Behavioral.ChainOfResponsibility
{
    public class ConcreteHandler1 : Handler
    {
        public override void HandleRequest(string request)
        {
            if (request == "Type1")
            {
                Console.WriteLine("Handled by ConcreteHandler1");
            }
            else
            {
                NextHandler?.HandleRequest(request);
            }
        }
    }
}
