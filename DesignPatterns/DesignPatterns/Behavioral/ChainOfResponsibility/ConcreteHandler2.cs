﻿namespace DesignPatterns.Behavioral.ChainOfResponsibility
{
    public class ConcreteHandler2 : Handler
    {
        public override void HandleRequest(string request)
        {
            if (request == "Type2")
            {
                Console.WriteLine("Handled by ConcreteHandler2");
            }
            else
            {
                NextHandler?.HandleRequest(request);
            }
        }
    }
}
