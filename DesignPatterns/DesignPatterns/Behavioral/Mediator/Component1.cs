﻿namespace DesignPatterns.Behavioral.Mediator
{
    public class Component1 : BaseComponent
    {
        public void DoA()
        {
            Console.WriteLine("Component1 does A.");
            Mediator.Notify(this, "A");
        }
    }
}
