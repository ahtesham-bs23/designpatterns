﻿namespace DesignPatterns.Behavioral.Mediator
{
    public class Component2 : BaseComponent
    {
        public void DoB()
        {
            Console.WriteLine("Component2 does B.");
        }
    }
}
