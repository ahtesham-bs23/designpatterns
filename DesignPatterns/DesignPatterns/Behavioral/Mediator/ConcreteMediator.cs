﻿namespace DesignPatterns.Behavioral.Mediator
{
    public class ConcreteMediator : IMediator
    {
        private Component1 _component1;
        private Component2 _component2;

        public ConcreteMediator(Component1 c1, Component2 c2)
        {
            _component1 = c1;
            _component2 = c2;
            _component1.SetMediator(this);
            _component2.SetMediator(this);
        }

        public void Notify(object sender, string eventCode)
        {
            if (eventCode == "A")
            {
                Console.WriteLine("Mediator reacts to A and triggers B.");
                _component2.DoB();
            }
        }
    }
}
