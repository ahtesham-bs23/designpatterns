﻿namespace DesignPatterns.Behavioral.Mediator
{
    public class BaseComponent
    {
        protected IMediator Mediator;

        public void SetMediator(IMediator mediator)
        {
            Mediator = mediator;
        }
    }
}
